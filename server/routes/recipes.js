const _Tools = require("../library/tools");
const request = require("../library/request");

module.exports = function(router) {

    // List all categories of recipes
    router.route("/categories").get(async (req, res) => {
        try {
            await _Tools.checkToken(req.headers.authorization);
            let categories = await request.getRows("Select * From category_recipe Where id_category != 0");
            res.status(200).json(categories);
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error getting the categories : ${error}.`);
        }
    });

    // List all recipes from a categorie
    router.route("/categories/:id/recipes").get(async (req, res) => {
        try {
            await _Tools.checkToken(req.headers.authorization);
            let userId = parseInt(req.headers.userid);
            let recipes = await _Tools.getRecipes(req.params.id, userId);
            res.status(200).json(recipes);
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error getting the categories : ${error}.`);
        }
    });

    // List all recipes
    router.route("/recipes").get(async (req, res) => {
        try {
            await _Tools.checkToken(req.headers.authorization);
            let userId = parseInt(req.headers.userid);
            let recipes = await _Tools.getAllRecipes(userId);
            res.status(200).json(recipes);
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error getting the recipes : ${error}.`);
        }
    });

    // Get a precise recipe
    router.route("/recipes/:id").get(async (req, res) => {
        try {
            await _Tools.checkToken(req.headers.authorization);
            let recipe = await _Tools.getRecipe(req.params.id);
            res.status(200).json(recipe);
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error getting the recipe : ${error}.`);
        }
    });

    // Create a recipe
    router.route("/recipe").post(async (req, res) => {
        try {
            await _Tools.checkToken(req.headers.authorization);
            let data = req.body;
            data["id_user"] = req.headers.userid;
            await _Tools.addRecipe(data);
            res.status(200).end();
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error deleting the recipe : ${error}.`);
        }
    });

    // Delete a recipe
    router.route("/recipes/:id").delete(async (req, res) => {
        try {
            await _Tools.checkToken(req.headers.authorization);
            // Check if the recipe belong to the current user
            let userId = parseInt(req.headers.userid);
            let exist = await request.getRow("SELECT 1 FROM recipes WHERE id_user=$1 AND id_recipe=$2", [userId, req.params.id]);
            if(!exist) {
                throw Error("This recipe doesn't belong to you, you can't delete it");
            }
            await _Tools.removeRecipe(req.params.id);
            res.status(200).end();
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error deleting the recipe : ${error}.`);
        }
    });

    // Update a recipe
    router.route("/recipes/:id").put(async (req, res) => {
        try {
            await _Tools.checkToken(req.headers.authorization);
            // Check if the recipe belong to the current user
            let userId = parseInt(req.headers.userid);
            let exist = await request.getRow("SELECT 1 FROM recipes WHERE id_user=$1 AND id_recipe=$2", [userId, req.params.id]);
            if(!exist) {
                throw Error("This recipe doesn't belong to you, you can't modify it");
            }
            let data = req.body;
            data["id_recipe"] = req.params.id;
            data["id_user"] = req.headers.userid;
            await _Tools.modifyRecipe(data);
            res.status(200).end();
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error updating the recipe : ${error}.`);
        }
    });

    // Send a recipe via mail
    router.route("/recipes/send/:id").post(async (req, res) => {
        try {
            await _Tools.checkToken(req.headers.authorization);
            // Check if the recipe belong to the current user
            let userId = parseInt(req.headers.userid);
            let exist = await request.getRow("SELECT 1 FROM recipes WHERE id_user=$1 AND id_recipe=$2", [userId, req.params.id]);
            if(!exist) {
                throw Error("This recipe doesn't belong to you, you can't send it");
            }
            await _Tools.sendRecipe(req.params.id, req.body.mail);
            res.status(200).end();
        } catch (error) {
            logger.error(error);
            res.status(400).send(`Error deleting the recipe : ${error}.`);
        }
    });

    // Add a recipe shared by email
    router.route("/recipes/addSharedRecipe/:id").post(async (req, res) => {
        try {
            await _Tools.checkToken(req.headers.authorization);
            // Check if the recipe belong to the current user
            let userId = parseInt(req.headers.userid);

            let recipe = await _Tools.getRecipe(req.params.id);
            recipe.id_user = userId;
            await _Tools.addRecipe(recipe);

            res.status(200).end();
        } catch (error) {
            logger.error(error);
            res.status(500).send(`Error adding the recipe : ${error}.`);
        }
    });

};