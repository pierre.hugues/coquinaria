const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const _Tools = require("../library/tools");
const utils = require("../library/utils");
const request = require("../library/request");
const config = require("../config/config.json");

module.exports = function(router) {

    // Register a new user
    router.route("/register").post(async (req, res) => {
        try {
            // Check fields consistency
            let { email, name, firstname, password } = req.body;
            if(!email || !name || !firstname || !password) return res.status(400).send("Missing fields");

            // Check doesn't already exists
            let exist = await request.getRow("SELECT 1 FROM Users WHERE email=$1", [email]);
            if(exist) {
                return res.status(409).send("This adress already exists");
            }
            let hash = await bcrypt.hash(password, 10);

            // Create user in database and recover its data
            let sql = "INSERT INTO Users (name, firstname, email, password) VALUES ($1, $2, $3, $4) RETURNING id_user";
            let user = await request.execSql(sql, [name, firstname, email, hash]);
            user = await request.getRow("SELECT * FROM Users WHERE id_user=$1", [user.id_user]);
            
            // Send verification email
            await utils.sendConfirmationMail(user.email);

            res.status(200).json(user);
        } catch (error) {
            logger.error(error);
            res.status(500).send(error.message || error.error || error.err || error);
        }
    });

    // Log a user
    router.route("/login").post(async (req, res) => {
        try {
            // Check fields consistency
            let { email, password } = req.body;
            if(!email || !password) return res.status(400).send("Missing fields");
    
            // Recover user
            const user = await request.getRow("SELECT * FROM Users WHERE email = $1 and active = $2", [email, true]);
            if (!user) return res.status(404).send("No user found with this email");
        
            // Check password
            const match = await bcrypt.compare(password, user.password);
            if (!match) return res.status(403).send("Passwords do not match");
    
            // Create JWT
            let jwtGenerated = jwt.sign(user, config.secret_key, {
                expiresIn: "1d",
            });
    
            res.status(200).json({
                jwt: jwtGenerated,
                account: user,
            });
        }  catch (error) {
            logger.error(error);
            res.status(500).send(error.message || error.error || error.err || error);
        }
    });

    // Verify a user
    router.route("/verif/:id").get(async (req, res) => {
        try {
            await request.execSql("UPDATE Users SET active=true WHERE activation_token=$1", [req.params.id]);
            await request.execSql("UPDATE Users SET activation_token=NULL WHERE activation_token=$1", [req.params.id]);
            res.status(200).end();
        } catch (error) {
            logger.error(error);
            res.status(400).send(error.message);
        }
    });

    // Resend a confimation mail to the adress of a user
    router.route("/resendMail/:id").get(async (req, res) => {
        try {
            let user = await _Tools.getUserId(req.params.id);
            if(user.isEmpty()) {
                throw Error("No user found");
            }
            await utils.sendConfirmationMail(user.email);

            res.status(200).end();
        } catch (error) {
            logger.error(error);
            res.status(500).send(error.message || error.error || error.err || error);
        }
    });

    // Check if the token is leggit
    router.route("/token/:token").get(async (req, res) => {
        try {
            await _Tools.checkToken(req.params.token);
            res.status(200).json(req.params.token);
        } catch (error) {
            logger.error(error);
            res.status(500).send(error.message || error.error || error.err || error);
        }
    });

    // Update a user
    router.route("/user").put(async (req, res) => {
        try {
            await _Tools.checkToken(req.headers.authorization);
            let user = req.body;

            const sql = "UPDATE Users SET firstname=$1, name=$2, email=$3, modified=current_timestamp where id_user=$4";
            await request.execSql(sql, [user.firstname, user.name, user.email, user.id_user]);

            user = await _Tools.getUserId(user.id_user);

            res.status(200).json(user);
        } catch (error) {
            logger.error(error);
            res.status(500).send(error.message || error.error || error.err || error);
        }
    });

    // Update a user
    router.route("/user/password").put(async (req, res) => {
        try {
            await _Tools.checkToken(req.headers.authorization);
            let user = req.body;

            const match = await bcrypt.compare(user.oldPassword, user.password);
            if (!match) return res.status(403).send("Passwords do not match");

            const hash = await bcrypt.hash(user.newPassword, 10);

            const sql = "UPDATE Users SET password=$1, modified=current_timestamp where id_user=$2";
            await request.execSql(sql, [hash, user.id_user]);

            user = await _Tools.getUserId(user.id_user);

            res.status(200).json(user);
        } catch (error) {
            logger.error(error);
            res.status(500).send(error.message || error.error || error.err || error);
        }
    });
};