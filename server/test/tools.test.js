const log4js = require("log4js");
// Configure the loggers
log4js.configure("./config/log4js.json");
global.logger = log4js.getLogger();
global.requestLogger = log4js.getLogger("request");

const { getUserId } = require("../library/tools")

describe('tools tests', () => {
    beforeAll(done => {
        done()
    });
    test('getUserId return something defined', async () => {
        expect(await getUserId(1)).toBeDefined();
    });
    test('getUserId has all users property', async () => {
        const user = await getUserId(1);
        expect(user).toHaveProperty('id_user');
        expect(user).toHaveProperty('name');
        expect(user).toHaveProperty('firstname');
        expect(user).toHaveProperty('email');
        expect(user).toHaveProperty('password');
        expect(user).toHaveProperty('created');
        expect(user).toHaveProperty('modified');
        expect(user).toHaveProperty('active');
        expect(user).toHaveProperty('activation_token');
    });
    test('getUserId return something undefined', async () => {
        expect(await getUserId()).toBeUndefined();
    });
    afterAll(done => {
        done()
    });
});