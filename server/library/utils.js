const mail = require("./mail");
const request = require("./request");
const config = require("../config/config.json");
const fs = require("fs").promises;

module.exports = {

    /**
     * Send a confirmation mail to the user
     * @param {String} email Recipient
     */
    sendConfirmationMail: async function sendConfirmationMail(email) {
        let rand = Math.floor((Math.random() * 100) + 54);
        let sql = "UPDATE Users SET activation_token=$1 WHERE email=$2";
        await request.execSql(sql, [rand, email]);
        let body = await fs.readFile(__dirname + "/../templates/mail/verif.html", "utf8");
        if(config.client.SERVER_PORT) {
            body = body.replace("$LINK", `${config.client.SERVER_PROTOCOL}://${config.client.SERVER_HOSTNAME}:${config.client.SERVER_PORT}/verify/${rand}`);
        } else {
            body = body.replace("$LINK", `${config.client.SERVER_PROTOCOL}://${config.client.SERVER_HOSTNAME}/verify/${rand}`);
        }
        await mail.SendMail({to: email, subject: "Veuillez confirmer votre adresse mail", body: body}, true);
    },

    

};