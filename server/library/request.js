const { Pool } = require("pg");
const config = require("../config/config.json");

const pool = new Pool(config.postgresql);

module.exports = {

    /**
     * Get a single row of data
     * @param {String} request Query
     * @param {String[]} parameters List of parameters
     */
    getRow: async function getRow(request, parameters) {
        const { rows } = await pool.query(request, parameters);
        return rows[0];
    },

    /**
     * Get all rows
     * @param {String} request Query
     * @param {String[]} parameters List of parameters
     */
    getRows: async function getRows(request, parameters) {
        const { rows } = await pool.query(request, parameters);
        return rows;
    },

    /**
     * Execute a sql request (UPDATE/CREATE/DELETE)
     * @param {String} request Query
     * @param {String[]} parameters List of parameters
     */
    execSql: async function execSql(request, parameters) {
        const { rows } = await pool.query(request, parameters);
        return rows[0];
    },

    /**
     * Start a transaction
     */
    startTransaction: async function startTransaction() {
        await pool.query("BEGIN");
        return;
    },

    /**
     * Commit a transaction
     */
    commit: async function commit() {
        await pool.query("COMMIT");
        return;
    },

    /**
     * Rollback a transaction
     */
    rollback: async function rollback() {
        await pool.query("ROLLBACK");
        return;
    },
};