const request = require("./request");
const _Mail = require("./mail");
const config = require("../config/config.json");
const fs = require("fs").promises;
const jwt = require("jsonwebtoken");

module.exports = {

    /**
     * Return an user based on his ID
     * @param {String} id_user Id to verify the existence
     */
    getUserId: async function(id_user) {
        try {
            let sql = "SELECT * FROM Users WHERE id_user=$1";
            let params = [id_user];
            let user = await request.getRow(sql, params);
            return user;
        } catch (error) {
            let msg = `[getUserId] ${error.message || error.error || error}`;
            logger.error(msg);
            throw msg;
        }
    },

    /**
     * Add a recipe to the database
     * @param {Object} data Data recovered from the form and the cookies
     */
    addRecipe: async function(data) {
        try {
            // Parse the data send
            let recipeName = data.name;
            let recipeTime = data.preparation_time;
            let description = data.description;
            let catRecipe = data.id_category;
            let instruction = data.instructions;
            let userID = data.id_user ? data.id_user : 11;
            let ings = data.ingredients;

            await request.startTransaction();

            // Insert the recipe in the database
            let sql = "INSERT INTO recipes (name, id_category, id_user, preparation_time, description, instructions) VALUES($1, $2, $3, $4, $5, $6) RETURNING id_recipe";
            let { id_recipe } = await request.execSql(sql, [recipeName, catRecipe, userID, recipeTime, description, instruction]);

            // Check if the ingredients already exists, if not add them to the ingredients database
            // and then add them to the list of ingredients for the recipe.
            for(let ing of ings) {
                sql = "SELECT * FROM ingredients WHERE name=$1";
                let result = await request.getRow(sql, [ing.name]);
                if(!result || !result.hasOwnProperty("id_ingredient")) {
                    sql = "INSERT INTO ingredients (name) VALUES ($1) RETURNING id_ingredient";
                    let { id_ingredient } = await request.execSql(sql, [ing.name]);
                    sql = "INSERT INTO ingredient_recipe (id_recipe, id_ingredient_recipe, quantity) VALUES ($1, $2, $3)";
                    await request.execSql(sql, [id_recipe, id_ingredient, ing.quantity]);
                } else {
                    let ingID = result["id_ingredient"];
                    sql = "INSERT INTO ingredient_recipe (id_recipe, id_ingredient_recipe, quantity) VALUES ($1, $2, $3)";
                    await request.execSql(sql, [id_recipe, ingID, ing.quantity]);
                }
            }

            await request.commit();
            return;
        } catch (error) {
            await request.rollback();
            let msg = `[addRecipe] ${error.message || error.error || error}`;
            logger.error(msg);
            throw msg;
        }
    },
    
    /**
     * Get the recipes from one categorie
     * @param {Number} id_category ID of the categorie
     * @param {Number} id_user ID of the user
     */
    getRecipes: async function(id_category, id_user) {
        try {
            let sql = "SELECT recipes.id_recipe, recipes.name, recipes.description, recipes.preparation_time " +
                    "FROM recipes " +
                    "LEFT JOIN category_recipe on recipes.id_category=category_recipe.id_category " +
                    "WHERE category_recipe.id_category=$1 " +
                    "AND recipes.id_user=$2 " +
                    "ORDER BY recipes.name";
            let data = await request.getRows(sql, [id_category, id_user]);
            return data;
        } catch (error) {
            let msg = `[getRecipes] ${error.message || error.error || error}`;
            logger.error(msg);
            throw msg;
        }
    },

    /**
     * Get all the recipes
     * @param {Number} id_user ID of the user
     */
    getAllRecipes: async function(id_user) {
        try {
            let sql = "SELECT DISTINCT recipes.id_recipe, recipes.name, recipes.description, recipes.preparation_time " +
                    "FROM recipes " +
                    "WHERE recipes.id_user=$1 " +
                    "ORDER BY recipes.name";
            let data = await request.getRows(sql, [id_user]);
            return data;
        } catch (error) {
            let msg = `[getAllRecipes] ${error.message || error.error || error}`;
            logger.error(msg);
            throw msg;
        }
    },

    /**
     * Get the information from a single recipe
     * @param {Number} id_recipe ID of the recipe
     */
    getRecipe: async function(id_recipe) {
        try {
            // Get recipe info
            let sql = "SELECT recipes.id_recipe, recipes.name, recipes.description, recipes.preparation_time, category_recipe.name as category_name, recipes.id_category, recipes.instructions " +
                    "FROM recipes " +
                    "LEFT JOIN category_recipe on recipes.id_category=category_recipe.id_category " +
                    "WHERE recipes.id_recipe=$1";
            let recipe = await request.getRow(sql, [id_recipe]);

            // Get the ingredients
            sql = "SELECT ingredient_recipe.quantity, ingredients.name " +
                    "FROM recipes " +
                    "RIGHT JOIN ingredient_recipe on ingredient_recipe.id_recipe=recipes.id_recipe " +
                    "RIGHT JOIN ingredients on ingredients.id_ingredient=ingredient_recipe.id_ingredient_recipe " +
                    "WHERE recipes.id_recipe=$1";
            let ingredients = await request.getRows(sql, [id_recipe]);

            recipe.instructions = recipe.instructions.replace(/(\\r\\n)|(\\n)/gm, "\n");
            recipe.ingredients = ingredients;

            return recipe;
        } catch (error) {
            let msg = `[getRecipe] ${error.message || error.error || error}`;
            logger.error(msg);
            throw msg;
        }
    },

    /**
     * Remove a recipe
     * @param {Number} id_recipe ID of the recipe
     */
    removeRecipe: async function(id_recipe) {
        try {
            // Remove the ingredients
            let sql = "DELETE FROM ingredient_recipe WHERE id_recipe=$1";
            await request.execSql(sql, [id_recipe]);
            // Remove the recipe
            sql = "DELETE FROM recipes WHERE id_recipe=$1";
            await request.execSql(sql, [id_recipe]);
            return;
        } catch (error) {
            let msg = `[removeRecipe] ${error.message || error.error || error}`;
            logger.error(msg);
            throw msg;
        }
    },

    /**
     * Modify a recipe from the database
     * @param {Object} data Data recovered from the form and the cookies
     */
    modifyRecipe: async function(data) {
        try {
            // Parse the data send
            let recipeName = data.name;
            let recipeTime = data.preparation_time;
            let description = data.description;
            let catRecipe = data.id_category;
            let instruction = data.instructions;
            let id_recipe = data.id_recipe;
            let ings = data.ingredients;

            // Update the recipe in the database
            let sql = "UPDATE recipes SET name=$1, id_category=$2, preparation_time=$3, description=$4, instructions=$5 WHERE id_recipe=$6";
            await request.execSql(sql, [recipeName, catRecipe, recipeTime, description, instruction, id_recipe]);

            // Remove all the ingredients linked to the recipe and then re-add them
            sql = "DELETE FROM ingredient_recipe WHERE id_recipe=$1";
            await request.execSql(sql, [id_recipe]);

            // Re-add the ingredients
            for(let ing of ings) {
                sql = "SELECT * FROM ingredients WHERE name=$1";
                let result = await request.getRow(sql, [ing.name]);
                if(!result || !result.hasOwnProperty("id_ingredient")) {
                    sql = "INSERT INTO ingredients (name) VALUES ($1) RETURNING id_ingredient";
                    let { id_ingredient } = await request.execSql(sql, [ing.name]);
                    sql = "INSERT INTO ingredient_recipe (id_recipe, id_ingredient_recipe, quantity) VALUES ($1, $2, $3)";
                    await request.execSql(sql, [id_recipe, id_ingredient, ing.quantity]);
                } else {
                    let ingID = result["id_ingredient"];
                    sql = "INSERT INTO ingredient_recipe (id_recipe, id_ingredient_recipe, quantity) VALUES ($1, $2, $3)";
                    await request.execSql(sql, [id_recipe, ingID, ing.quantity]);
                }
            }
            return;
        } catch (error) {
            let msg = `[modifyRecipe] ${error.message || error.error || error}`;
            logger.error(msg);
            throw msg;
        }
    },

    /**
     * Share a recipe to an other user with his email adress
     * @param {String} id_recipe ID of the recipe
     * @param {String} mail Recipient of the recipe
     */
    sendRecipe: async function(id_recipe, mail) {
        return new Promise(async (resolve, reject) => {
            try {
                let recipe = await this.getRecipe(id_recipe);

                let body = await fs.readFile(__dirname + "/../templates/mail/recipe.html", "utf8");

                body = body.replace("$CATEGORIE", recipe.category_name.toUpperCase());
                body = body.replace("$NAME", recipe.name);
                body = body.replace("$DESCRIPTION", recipe.description);
                body = body.replace("$TEMPS", recipe.preparation_time);
                body = body.replace("$INSTRUCTION", recipe.instructions.replace(/\n/gmi, "<br />"));

                for(let i = 0 ; i < recipe.ingredients.length ; i++) {
                    let ing = recipe.ingredients[i];
                    body = body.replace("$ING", "<li>" + ing["quantity"] + " " + ing["name"] + "</li>" + "<hr width=\"100%\" size=1 align=\"left\">$ING");
                }
                body = body.replace("$ING", "");

                if(config.client.SERVER_PORT) {
                    body = body.replace("$LINK", `${config.client.SERVER_PROTOCOL}://${config.client.SERVER_HOSTNAME}:${config.client.SERVER_PORT}/addSharedRecipe/${id_recipe}`);
                } else {
                    body = body.replace("$LINK", `${config.client.SERVER_PROTOCOL}://${config.client.SERVER_HOSTNAME}/addSharedRecipe/${id_recipe}`);
                }

                await _Mail.SendMail({to: mail, subject: "Recette partagée", body: body}, true),
                resolve(true);
            } catch (error) {
                let msg = `[sendRecipe] ${error.message || error.error || error}`;
                reject(msg);
            }
        });
    },

    /**
     * Verify if a token is leggit
     * @param {String} token Token to verify
     */
    checkToken: async function(token) {
        return new Promise(async (resolve, reject) => {
            try {
                if(!token) throw Error("No token provided");
                await new Promise((rs, rj) => {
                    jwt.verify(token, config.secret_key, (err, decoded) => {
                        if(err) rj(err);
                        else rs(decoded);
                    });
                });
                resolve();
            } catch (error) {
                let msg = `[checkToken] ${error.message || error.error || error}`;
                reject(msg);
            }
        });
    },

};