const nodemailer = require("nodemailer");

let config;
let transporter;

try {
    config = require("../config/config.json");
    transporter = nodemailer.createTransport(config.mail);
    transporter.verify((error, sucess) => {
        if(error) logger.error(`Error connecting to mailing service : ${error}`);
        else logger.info("Mail connection is successful");
    });
} catch (error) {
    logger.error("Mail config missing : no transport created");
}

module.exports = {

    /**
     * Send an email
     * @param {Object} params Necessary parameters to send the mail
     * @param {String} params.from Sender of the mail
     * @param {String|Array<String>} params.to Recipient of the mail (Comma separated list or an array)
     * @param {String|Array<String>} params.cc Recipient in copy of the mail (Comma separated list or an array)
     * @param {String|Array<String>} params.bcc Recipient in hidden copy of the mail (Comma separated list or an array)
     * @param {String} params.subjec Subject of the mail
     * @param {String} params.body Content of the mail
     * @param {*} params.attachments Attachments of the mail (Array of attachments which are object). Check the wiki for more informations
     * @param {Boolean} isHtml If the content is html
     * @see https://nodemailer.com/message/
     */
    SendMail: async function(params, isHtml) {
        try {
            let mailOptions = {
                from: params && params.from ? params.from : config.mail.auth.user,
                to: params && params.to ? params.to : "",
                subject: params && params.subject ? params.subject : "",
                cc: params && params.cc ? params.cc : "",
                bcc: params && params.bcc ? params.bcc : "",
                attachments: params && params.attachments ? params.attachments : "",
            };
            isHtml ? mailOptions.html = params.body : mailOptions.text = params.body;
            await transporter.sendMail(mailOptions);
            logger.info(`Mail sent : [${mailOptions.subject}] to <${mailOptions.to}>`);
        } catch (error) {
            let msg = `[SendMail] ${error.message || error.error || error}`;
            logger.error(msg);
        }
    },

};