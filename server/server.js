const express = require("express");
const cors = require("cors");
const log4js = require("log4js");

const config = require("./config/config.json");

// Configure the loggers
log4js.configure("./config/log4js.json");
global.logger = log4js.getLogger();
global.requestLogger = log4js.getLogger("request");

require("./library/prototype");

const app = express();

// Configure the roads
const router = express.Router();
require("./routes/default")(router);
require("./routes/user")(router);
require("./routes/recipes")(router);

app.use(cors());
app.use(express.json());
app.use("/", router);

const port = config && config.server && config.server.SERVER_PORT || 4000;
app.listen(port, async () => {
    logger.info(`HTTP server listening on port ${port}`);
});

process.on('SIGINT', () => {
    process.exit();
});

process.on('SIGTERM', () => {
    process.exit();
});