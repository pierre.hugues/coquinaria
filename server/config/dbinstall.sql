/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE TABLE IF NOT EXISTS category_recipe (
  id_category SERIAL NOT NULL,
  label varchar(45) NOT NULL,
  name varchar(45) NOT NULL,
  PRIMARY KEY (id_category)
);

INSERT INTO category_recipe (id_category, label, name) VALUES
	(0, 'erreur', 'erreur'),
	(1, 'entrées', 'entrees'),
	(2, 'plats', 'plats'),
	(3, 'desserts', 'desserts'),
	(4, 'gâteaux', 'gateaux'),
	(5, 'boulangerie', 'boulangerie'),
	(6, 'sauces', 'sauces')
ON CONFLICT (id_category) DO NOTHING;

CREATE TABLE IF NOT EXISTS users (
  id_user SERIAL NOT NULL,
  name varchar(45) NOT NULL,
  firstname varchar(45) NOT NULL,
  email varchar(100) NOT NULL,
  password varchar(255) NOT NULL,
  created timestamp with time zone DEFAULT current_timestamp,
  modified timestamp with time zone,
  active boolean DEFAULT false,
  activation_token int DEFAULT NULL,
  PRIMARY KEY (id_user)
);

INSERT INTO users (id_user, name, firstname, email, password, created, modified, active, activation_token) VALUES
	(1, 'Test', 'User', 'test@gmail', '$2b$10$Q21F/IM38pI6F/lgThIEyedPdENIwYPV95jH3zObzd0Vm71pXyTQy', '2018-11-03', NULL, true, NULL)
ON CONFLICT (id_user) DO NOTHING;

CREATE TABLE IF NOT EXISTS recipes (
  id_recipe SERIAL NOT NULL,
  name text NOT NULL,
  id_category int NOT NULL,
  preparation_time int NOT NULL DEFAULT '30',
  instructions text NOT NULL,
  id_user int NOT NULL,
  description text NOT NULL,
  PRIMARY KEY (id_recipe),
  CONSTRAINT fk_recipe_1 FOREIGN KEY (id_category) REFERENCES category_recipe (id_category) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_recipe_2 FOREIGN KEY (id_user) REFERENCES users (id_user) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS ingredients (
  id_ingredient SERIAL NOT NULL,
  name text NOT NULL,
  PRIMARY KEY (id_ingredient)
);

CREATE TABLE IF NOT EXISTS ingredient_recipe (
  id_recipe int NOT NULL,
  id_ingredient_recipe int NOT NULL,
  quantity text NOT NULL,
  PRIMARY KEY (id_recipe,id_ingredient_recipe),
  CONSTRAINT fk_ingredient_recipe_1 FOREIGN KEY (id_ingredient_recipe) REFERENCES ingredients (id_ingredient) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT fk_ingredient_recipe_2 FOREIGN KEY (id_recipe) REFERENCES recipes (id_recipe) ON DELETE NO ACTION ON UPDATE NO ACTION
);

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
